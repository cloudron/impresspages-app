#!/bin/bash

set -eu

cd /app/code/

if [ ! -d "/app/data/file" ]; then
    echo "=> Create initial file folder"
    cp -rf /app/code/file_save /app/data/file
fi

if [ ! -f "/app/data/config.php" ]; then
    echo "=> Create initial config.php"
    cp /app/code/config.php.template /app/data/config.php

    echo "=> Seed the database"
    mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} < /app/code/seed.sql
fi

echo "=> Update config"
sed -e "s/'hostname' => '.*',/'hostname' => '${MYSQL_HOST}',/" \
    -e "s/'username' => '.*',/'username' => '${MYSQL_USERNAME}',/" \
    -e "s/'password' => '.*',/'password' => '${MYSQL_PASSWORD}',/" \
    -e "s/'tablePrefix' => '.*',/'tablePrefix' => 'ip_',/" \
    -e "s/'database' => '.*',/'database' => '${MYSQL_DATABASE}',/" \
    -i "/app/data/config.php"

# Enable this section to be able to take a db dump for package creation
# cp /app/code/config.php.original /app/data/config.php

echo "=> Setup permissions"
chown -R www-data.www-data /app/data /run/impresspages

echo "=> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
