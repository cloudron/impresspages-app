FROM cloudron/base:0.10.0
MAINTAINER ImpressPages Developers <support@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y php libapache2-mod-php php-mcrypt && rm -r /var/cache/apt /var/lib/apt/lists

RUN curl -L https://github.com/impresspages/ImpressPages/archive/v4.7.0.tar.gz | tar -xz --strip-components 1 -f -

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN echo "Listen 8000" > /etc/apache2/ports.conf
ADD apache2-impresspages.conf /etc/apache2/sites-available/impresspages.conf
RUN ln -sf /etc/apache2/sites-available/impresspages.conf /etc/apache2/sites-enabled/impresspages.conf
RUN a2enmod rewrite headers

# mod_php config
RUN sed -e 's/upload_max_filesize = .*/upload_max_filesize = 8M/' \
        -e 's,;session.save_path.*,session.save_path = "/run/impresspages/sessions",' \
        -i /etc/php/7.0/apache2/php.ini

RUN mv /app/code/file /app/code/file_save && ln -s /app/data/file /app/code/file
RUN mv /app/code/config.php /app/code/config.php.original && ln -s /app/data/config.php /app/code/config.php

RUN mkdir -p /run/impresspages/sessions

ADD start.sh config.php.template seed.sql /app/code/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/code/start.sh" ]
